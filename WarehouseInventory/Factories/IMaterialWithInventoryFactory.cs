﻿using WarehouseInventory.Aggregates;
using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public interface IMaterialWithInventoryFactory
    {
        MaterialWithInventory Create(Material material, int inventory);
    }
}
