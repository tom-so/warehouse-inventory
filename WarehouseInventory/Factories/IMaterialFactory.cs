﻿using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public interface IMaterialFactory
    {
        Material Create(string code, string name);
    }
}
