﻿using WarehouseInventory.Aggregates;
using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public class MaterialWithInventoryFactory : IMaterialWithInventoryFactory
    {
        public MaterialWithInventory Create(Material material, int inventory)
        {
            return new MaterialWithInventory(material, inventory);
        }
    }
}
