﻿using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public class MaterialFactory : IMaterialFactory
    {
        public Material Create(string code, string name)
        {
            return new Material(name, code);
        }
    }
}
