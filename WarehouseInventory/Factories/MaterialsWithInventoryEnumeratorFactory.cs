﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Enumerators;

namespace WarehouseInventory.Factories
{
    public class MaterialsWithInventoryEnumeratorFactory : IMaterialsWithInventoryEnumeratorFactory
    {
        public MaterialsWithInventoryEnumerator Create(IEnumerable<MaterialWithInventory> materialsWithInventory)
        {
            return new MaterialsWithInventoryEnumerator(materialsWithInventory);
        }
    }
}
