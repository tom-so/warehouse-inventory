﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Enumerators;

namespace WarehouseInventory.Factories
{
    public interface IWarehousesWithMaterialsInventoryEnumeratorFactory
    {
        WarehousesWithMaterialsInventoryEnumerator Create(
            IEnumerable<WarehouseWithMaterialsInventory> warehousesWithMaterialsInventory);
    }
}
