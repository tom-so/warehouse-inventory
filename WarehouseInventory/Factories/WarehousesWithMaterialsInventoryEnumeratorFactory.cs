﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Enumerators;

namespace WarehouseInventory.Factories
{
    public class WarehousesWithMaterialsInventoryEnumeratorFactory : IWarehousesWithMaterialsInventoryEnumeratorFactory
    {
        public WarehousesWithMaterialsInventoryEnumerator Create(
            IEnumerable<WarehouseWithMaterialsInventory> warehousesWithMaterialsInventory)
        {
            return new WarehousesWithMaterialsInventoryEnumerator(warehousesWithMaterialsInventory);
        }
    }
}
