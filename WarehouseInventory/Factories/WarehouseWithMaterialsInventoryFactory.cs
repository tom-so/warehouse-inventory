﻿using System;
using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Entities;
using WarehouseInventory.Enumerators;

namespace WarehouseInventory.Factories
{
    public class WarehouseWithMaterialsInventoryFactory : IWarehouseWithMaterialsInventoryFactory
    {
        private readonly IMaterialsWithInventoryEnumeratorFactory _materialsWithInventoryEnumeratorFactory;

        public WarehouseWithMaterialsInventoryFactory(
            IMaterialsWithInventoryEnumeratorFactory materialsWithInventoryEnumeratorFactory)
        {
            _materialsWithInventoryEnumeratorFactory = materialsWithInventoryEnumeratorFactory ?? throw new ArgumentNullException(nameof(materialsWithInventoryEnumeratorFactory));
        }

        public WarehouseWithMaterialsInventory Create(
            Warehouse warehouse, 
            List<MaterialWithInventory> materialsWithInventory)
        {
            MaterialsWithInventoryEnumerator materialsWithInventoryEnumerable = 
                _materialsWithInventoryEnumeratorFactory.Create(materialsWithInventory);

            return new WarehouseWithMaterialsInventory(warehouse, materialsWithInventoryEnumerable);
        }
    }
}
