﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Enumerators;

namespace WarehouseInventory.Factories
{
    public interface IMaterialsWithInventoryEnumeratorFactory
    {
        MaterialsWithInventoryEnumerator Create(IEnumerable<MaterialWithInventory> materialsWithInventory);
    }
}
