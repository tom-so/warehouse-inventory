﻿using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public interface IWarehouseFactory
    {
        Warehouse Create(string warehouseCode);
    }
}
