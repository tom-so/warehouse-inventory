﻿using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public class WarehouseFactory : IWarehouseFactory
    {
        public Warehouse Create(string warehouseCode)
        {
            return new Warehouse(warehouseCode);
        }
    }
}
