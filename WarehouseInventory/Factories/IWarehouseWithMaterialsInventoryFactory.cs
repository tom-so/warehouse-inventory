﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Entities;

namespace WarehouseInventory.Factories
{
    public interface IWarehouseWithMaterialsInventoryFactory
    {
        WarehouseWithMaterialsInventory Create(Warehouse warehouse, List<MaterialWithInventory> materialsWithInventory);
    }
}
