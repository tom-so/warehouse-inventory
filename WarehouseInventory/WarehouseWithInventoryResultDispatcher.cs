﻿using System;
using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Factories;
using WarehouseInventory.InputProcessing.Parsers;
using WarehouseInventory.OutputProcessing;
using WarehouseInventory.Repositories;

namespace WarehouseInventory
{
    public class WarehouseWithInventoryResultDispatcher
    {
        private readonly IConsoleInputParser _consoleInputParser;
        private readonly IWarehouseWithMaterialsInventoryRepository _warehouseWithMaterialsInventoryRepository;
        private readonly IWarehousesWithMaterialsInventoryEnumeratorFactory _warehousesWithMaterialsInventoryEnumeratorFactory;
        private readonly IConsoleOutputFormatter _consoleOutputFormatter;

        public WarehouseWithInventoryResultDispatcher(
            IConsoleInputParser consoleInputParser,
            IWarehouseWithMaterialsInventoryRepository warehouseWithMaterialsInventoryRepository,
            IWarehousesWithMaterialsInventoryEnumeratorFactory warehousesWithMaterialsInventoryEnumeratorFactory,
            IConsoleOutputFormatter consoleOutputFormatter)
        {
            _consoleInputParser = consoleInputParser ?? throw new ArgumentNullException(nameof(consoleInputParser));
            _warehouseWithMaterialsInventoryRepository = warehouseWithMaterialsInventoryRepository ?? throw new ArgumentNullException(nameof(warehouseWithMaterialsInventoryRepository));
            _warehousesWithMaterialsInventoryEnumeratorFactory = warehousesWithMaterialsInventoryEnumeratorFactory ?? throw new ArgumentNullException(nameof(warehousesWithMaterialsInventoryEnumeratorFactory));
            _consoleOutputFormatter = consoleOutputFormatter ?? throw new ArgumentNullException(nameof(consoleOutputFormatter));
        }

        public void Dispatch()
        {
            _consoleInputParser.Parse();

            IEnumerable<WarehouseWithMaterialsInventory> warehousesWithMaterialsInventory = 
                _warehousesWithMaterialsInventoryEnumeratorFactory.Create(_warehouseWithMaterialsInventoryRepository.GetAll());

            _consoleOutputFormatter.Format(warehousesWithMaterialsInventory);
        }
    }
}
