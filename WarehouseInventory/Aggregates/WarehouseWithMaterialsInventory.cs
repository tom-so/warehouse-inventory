﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarehouseInventory.Entities;

namespace WarehouseInventory.Aggregates
{
    public class WarehouseWithMaterialsInventory
    {
        public WarehouseWithMaterialsInventory(
            Warehouse warehouse,
            IEnumerable<MaterialWithInventory> materialWithInventories)
        {
            MaterialsWithInventory = materialWithInventories ?? throw new ArgumentNullException(nameof(materialWithInventories));
            Warehouse = warehouse ?? throw new ArgumentNullException(nameof(warehouse));
            WarehouseTotalInventory = MaterialsWithInventory.Sum(m => m.Inventory);
        }

        public Warehouse Warehouse { get; }
        public IEnumerable<MaterialWithInventory> MaterialsWithInventory { get; }
        public int WarehouseTotalInventory { get; }
    }
}
