﻿using System;
using WarehouseInventory.Entities;

namespace WarehouseInventory.Aggregates
{
    public class MaterialWithInventory
    {
        public MaterialWithInventory(Material material, int inventory)
        {
            Material = material ?? throw new ArgumentNullException(nameof(material));
            Inventory = inventory;
        }

        public Material Material { get; }
        public int Inventory { get; }
    }
}
