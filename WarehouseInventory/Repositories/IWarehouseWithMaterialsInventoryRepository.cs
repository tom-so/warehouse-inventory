﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;

namespace WarehouseInventory.Repositories
{
    public interface IWarehouseWithMaterialsInventoryRepository
    {
        void Add(string warehouseCode, MaterialWithInventory materialWithInventory);
        IEnumerable<WarehouseWithMaterialsInventory> GetAll();
    }
}
