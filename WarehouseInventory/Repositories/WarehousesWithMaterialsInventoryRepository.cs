﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;
using WarehouseInventory.Factories;

namespace WarehouseInventory.Repositories
{
    public class WarehouseWithMaterialsInventoryRepository : IWarehouseWithMaterialsInventoryRepository
    {
        private readonly IDictionary<string, List<MaterialWithInventory>> _warehouseWithMaterialsInventory;
        private readonly IWarehouseWithMaterialsInventoryFactory _warehouseWithMaterialsInventoryFactory;
        private readonly IWarehouseFactory _warehouseFactory;

        public WarehouseWithMaterialsInventoryRepository(
            IWarehouseWithMaterialsInventoryFactory warehouseWithMaterialsInventoryFactory,
            IWarehouseFactory warehouseFactory)
        {
            _warehouseWithMaterialsInventory = new Dictionary<string, List<MaterialWithInventory>>();

            _warehouseWithMaterialsInventoryFactory = warehouseWithMaterialsInventoryFactory ?? throw new System.ArgumentNullException(nameof(warehouseWithMaterialsInventoryFactory));
            _warehouseFactory = warehouseFactory ?? throw new System.ArgumentNullException(nameof(warehouseFactory));
        }

        public void Add(string warehouseCode, MaterialWithInventory materialWithInventory)
        {
            if (_warehouseWithMaterialsInventory.ContainsKey(warehouseCode))
            {
                _warehouseWithMaterialsInventory[warehouseCode].Add(materialWithInventory);
            }
            else
            {
                _warehouseWithMaterialsInventory[warehouseCode] = new List<MaterialWithInventory> { materialWithInventory };
            }
        }

        public IEnumerable<WarehouseWithMaterialsInventory> GetAll()
        {
            foreach (var warehouseWithInventory in _warehouseWithMaterialsInventory)
            {
                var warehouse = _warehouseFactory.Create(warehouseWithInventory.Key);

                yield return _warehouseWithMaterialsInventoryFactory.Create(warehouse, warehouseWithInventory.Value);
            }
        }
    }
}
