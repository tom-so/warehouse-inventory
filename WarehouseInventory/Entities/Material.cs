﻿namespace WarehouseInventory.Entities
{
    public class Material
    {
        public Material(string name, string code)
        {
            Name = name;
            Code = code;
        }

        public string Name { get; }
        public string Code { get; }
    }
}
