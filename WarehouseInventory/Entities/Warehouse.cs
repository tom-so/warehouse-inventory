﻿using System;

namespace WarehouseInventory.Entities
{
    public sealed class Warehouse : IEquatable<Warehouse>
    {
        public Warehouse(string code)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
        }

        public string Code { get; }

        public bool Equals(Warehouse other) => Code == other.Code;

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;

            return Equals((Warehouse)obj);
        }

        public override int GetHashCode() => Code.GetHashCode();
    }
}
