﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WarehouseInventory.Aggregates;

namespace WarehouseInventory.Enumerators
{
    public class WarehousesWithMaterialsInventoryEnumerator : IEnumerable<WarehouseWithMaterialsInventory>
    {
        private readonly IEnumerable<WarehouseWithMaterialsInventory> _warehousesWithMaterialsInventory;

        public WarehousesWithMaterialsInventoryEnumerator(
            IEnumerable<WarehouseWithMaterialsInventory> warehousesWithMaterialsInventory)
        {
            _warehousesWithMaterialsInventory = warehousesWithMaterialsInventory ?? throw new ArgumentNullException(nameof(warehousesWithMaterialsInventory));
        }

        public IEnumerator<WarehouseWithMaterialsInventory> GetEnumerator()
        {
            foreach (WarehouseWithMaterialsInventory warehouseWithMaterialsInventory in GetOrderedWarehouses())
            {
                yield return warehouseWithMaterialsInventory;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private IOrderedEnumerable<WarehouseWithMaterialsInventory> GetOrderedWarehouses()
        {
            return _warehousesWithMaterialsInventory
                .OrderByDescending(m => m.WarehouseTotalInventory)
                .ThenByDescending(m => m.Warehouse.Code);
        }
    }
}
