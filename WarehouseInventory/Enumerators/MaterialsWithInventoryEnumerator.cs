﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WarehouseInventory.Aggregates;

namespace WarehouseInventory.Enumerators
{
    public class MaterialsWithInventoryEnumerator : IEnumerable<MaterialWithInventory>
    {
        private readonly IEnumerable<MaterialWithInventory> _materialsWithInventory;

        public MaterialsWithInventoryEnumerator(IEnumerable<MaterialWithInventory> materialsWithInventory)
        {
            _materialsWithInventory = materialsWithInventory ?? throw new ArgumentNullException(nameof(materialsWithInventory));
        }

        public IEnumerator<MaterialWithInventory> GetEnumerator()
        {
            foreach (MaterialWithInventory materialWithInventory in _materialsWithInventory.OrderBy(mwi => mwi.Material.Code))
            {
                yield return materialWithInventory;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
