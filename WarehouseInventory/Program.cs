﻿using WarehouseInventory.Factories;
using WarehouseInventory.InputProcessing.ConsoleWrappers;
using WarehouseInventory.InputProcessing.Parsers;
using WarehouseInventory.OutputProcessing;
using WarehouseInventory.Repositories;

namespace WarehouseInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            // to DI container
            var repository = new WarehouseWithMaterialsInventoryRepository(
                new WarehouseWithMaterialsInventoryFactory(
                    new MaterialsWithInventoryEnumeratorFactory()),
                    new WarehouseFactory()); // per request scope / singleton in this case
            var programDispatcher = new WarehouseWithInventoryResultDispatcher(
                new ConsoleInputParser(
                    new ConsoleReader(),
                    new InputLineParser(
                        repository,
                        new MaterialWithInventoryFactory(),
                        new MaterialFactory())),
                repository,
                new WarehousesWithMaterialsInventoryEnumeratorFactory(),
                new ConsoleOutputFormatter(
                    new ConsoleOutputWriter()));

            programDispatcher.Dispatch();
        }
    }
}
