﻿using System.Collections.Generic;
using System.Linq;
using WarehouseInventory.Aggregates;

namespace WarehouseInventory.OutputProcessing
{
    public class ConsoleOutputFormatter : IConsoleOutputFormatter
    {
        private readonly IConsoleOutputWriter _consoleOutputWriter;

        public ConsoleOutputFormatter(IConsoleOutputWriter consoleOutputWriter)
        {
            _consoleOutputWriter = consoleOutputWriter ?? throw new System.ArgumentNullException(nameof(consoleOutputWriter));
        }

        public void Format(IEnumerable<WarehouseWithMaterialsInventory> warehousesWithMaterialsInventory)
        {
            List<WarehouseWithMaterialsInventory> warehousesWithMaterialsInventoryList = 
                warehousesWithMaterialsInventory.ToList();

            int iterator = 0;
            foreach (WarehouseWithMaterialsInventory warehouseWithMaterialsInventory in warehousesWithMaterialsInventoryList)
            {
                _consoleOutputWriter.Write($"{warehouseWithMaterialsInventory.Warehouse.Code} (total {warehouseWithMaterialsInventory.WarehouseTotalInventory})");
                foreach (var materialWithInventory in warehouseWithMaterialsInventory.MaterialsWithInventory)
                {
                    _consoleOutputWriter.Write($"{materialWithInventory.Material.Code}: {materialWithInventory.Inventory}");
                }

                if (iterator != warehousesWithMaterialsInventoryList.Count - 1) _consoleOutputWriter.WriteEmptyLine();
            }
        }
    }
}
