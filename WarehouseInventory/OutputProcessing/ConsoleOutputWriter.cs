﻿using System;

namespace WarehouseInventory.OutputProcessing
{
    public class ConsoleOutputWriter : IConsoleOutputWriter
    {
        public void Write(string outputMessage)
        {
            Console.WriteLine(outputMessage);
        }

        public void WriteEmptyLine()
        {
            Console.WriteLine();
        }
    }
}
