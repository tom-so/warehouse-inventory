﻿namespace WarehouseInventory.OutputProcessing
{
    public interface IConsoleOutputWriter
    {
        void Write(string outputMessage);
        void WriteEmptyLine();
    }
}
