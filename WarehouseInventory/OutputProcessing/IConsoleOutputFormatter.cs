﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;

namespace WarehouseInventory.OutputProcessing
{
    public interface IConsoleOutputFormatter
    {
        void Format(IEnumerable<WarehouseWithMaterialsInventory> warehouseWithMaterialsInventories);
    }
}
