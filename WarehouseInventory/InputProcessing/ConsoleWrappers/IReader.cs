﻿namespace WarehouseInventory.InputProcessing.ConsoleWrappers
{
    public interface IReader
    {
        string ReadLine();
    }
}
