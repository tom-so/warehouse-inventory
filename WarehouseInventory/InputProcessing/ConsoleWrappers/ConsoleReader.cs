﻿using System;

namespace WarehouseInventory.InputProcessing.ConsoleWrappers
{
    public class ConsoleReader : IReader
    {
        public string ReadLine() => Console.ReadLine();
    }
}
