﻿namespace WarehouseInventory.InputProcessing.Splitters
{
    public static class InputLineSplitterFactory
    {
        public static IInputLineSplitter Create(char separator)
        {
            return new InputLineSplitter(separator);
        }
    }
}
