﻿using System;

namespace WarehouseInventory.InputProcessing.Splitters
{
    public class InputLineSplitter : IInputLineSplitter
    {
        private readonly char _separator;

        public InputLineSplitter(char separator)
        {
            _separator = separator;
        }

        public string[] Split(string rawInputLine)
        {
            return rawInputLine.Split(_separator);
        }
    }
}
