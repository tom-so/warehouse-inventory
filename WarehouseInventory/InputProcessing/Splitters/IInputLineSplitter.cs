﻿namespace WarehouseInventory.InputProcessing.Splitters
{
    public interface IInputLineSplitter
    {
        string[] Split(string rawInputLine);
    }
}