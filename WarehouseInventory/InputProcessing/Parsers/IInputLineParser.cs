﻿namespace WarehouseInventory.InputProcessing.Parsers
{
    public interface IInputLineParser
    {
        void ParseAndAddToRepository(string rawInputLine);
    }
}