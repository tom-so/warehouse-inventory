﻿using System.Collections.Generic;
using WarehouseInventory.Aggregates;

namespace WarehouseInventory.InputProcessing.Parsers
{
    public class InputLineParserResult
    {
        public string WarehouseCode { get; set; }
        public List<MaterialWithInventory> MaterialsWithInventory { get; set; }
    }
}