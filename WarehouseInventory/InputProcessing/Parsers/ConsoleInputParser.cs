﻿using System;
using System.Linq;
using WarehouseInventory.InputProcessing.ConsoleWrappers;

namespace WarehouseInventory.InputProcessing.Parsers
{
    public class ConsoleInputParser : IConsoleInputParser
    {
        private readonly IReader _reader;
        private readonly IInputLineParser _inputLineParser;

        public ConsoleInputParser(
            IReader reader,
            IInputLineParser inputLineParser)
        {
            _reader = reader ?? throw new ArgumentNullException(nameof(reader));
            _inputLineParser = inputLineParser ?? throw new ArgumentNullException(nameof(inputLineParser));
        }

        public void Parse()
        {
            while (true)
            {
                string line = _reader.ReadLine();

                if (IsEmptyLine(line)) break;
                if (IsCommentLine(line)) continue;

                _inputLineParser.ParseAndAddToRepository(line);
            }
        }

        private bool IsEmptyLine(string line) => line == null || line == string.Empty;

        private bool IsCommentLine(string line) => line.Trim().First() == '#';
    }
}
