﻿namespace WarehouseInventory.InputProcessing.Parsers
{
    public interface IConsoleInputParser
    {
        void Parse();
    }
}