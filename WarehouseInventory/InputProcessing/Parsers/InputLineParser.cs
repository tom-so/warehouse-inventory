﻿using WarehouseInventory.Aggregates;
using WarehouseInventory.Entities;
using WarehouseInventory.Factories;
using WarehouseInventory.InputProcessing.Splitters;
using WarehouseInventory.Repositories;

namespace WarehouseInventory.InputProcessing.Parsers
{
    public class InputLineParser : IInputLineParser
    {
        private readonly IWarehouseWithMaterialsInventoryRepository _repository;
        private readonly IMaterialWithInventoryFactory _materialWithInventoryFactory;
        private readonly IMaterialFactory _materialFactory;

        public InputLineParser(
            IWarehouseWithMaterialsInventoryRepository repository,
            IMaterialWithInventoryFactory materialWithInventoryFactory,
            IMaterialFactory materialFactory)
        {
            _repository = repository ?? throw new System.ArgumentNullException(nameof(repository));
            _materialWithInventoryFactory = materialWithInventoryFactory ?? throw new System.ArgumentNullException(nameof(materialWithInventoryFactory));
            _materialFactory = materialFactory ?? throw new System.ArgumentNullException(nameof(materialFactory));
        }

        public void ParseAndAddToRepository(string rawInputLine)
        {
            string[] materialInWarehouseData = InputLineSplitterFactory.Create(';').Split(rawInputLine);
            Material material = CreateMaterial(materialInWarehouseData);

            string[] materialInWarehouses = SplitToWarehouseInventory(materialInWarehouseData[2]);
            foreach (string materialInWarehouse in materialInWarehouses)
            {
                string[] warehouseCodeAndMaterialInventoryPair = SplitToWarehouseCodeAndMaterialInventoryPair(materialInWarehouse);
                
                string warehouseCode = warehouseCodeAndMaterialInventoryPair[0];

                int materialInventory = int.Parse(warehouseCodeAndMaterialInventoryPair[1]);
                MaterialWithInventory materialWithInventory = CreateMaterialWithInventory(material, materialInventory);

                _repository.Add(warehouseCode, materialWithInventory);
            }
        }

        private string[] SplitToWarehouseCodeAndMaterialInventoryPair(string materialInWarehouse)
        {
            return InputLineSplitterFactory.Create(',').Split(materialInWarehouse);
        }

        private string[] SplitToWarehouseInventory(string linePartitionWithInventoryInWarehouses)
        {
            return InputLineSplitterFactory.Create('|').Split(linePartitionWithInventoryInWarehouses);
        }

        private MaterialWithInventory CreateMaterialWithInventory(Material material, int inventory)
        {
            return _materialWithInventoryFactory.Create(material, inventory);
        }

        private Material CreateMaterial(string[] materialInWarehouseData)
        {
            string materialName = materialInWarehouseData[0];
            string materialCode = materialInWarehouseData[1];

            return _materialFactory.Create(materialCode, materialName);
        }
    }
}
